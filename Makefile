# DEFAULT
PHONY=build clean

TARGET_VERSION=
CORE_COMMIT=
UI_COMMIT=

DIST_VERSION=uxmp-${TARGET_VERSION}

DIST_CORE=dist/core
DIST_UI=dist/ui

BUILD_PATH=build/${DIST_VERSION}
BUILD_CORE=${BUILD_PATH}/core
BUILD_UI=${BUILD_PATH}/ui

TARBALL=${DIST_VERSION}.tar.gz

build:clean
	@[ "${CORE_COMMIT}" ] || ( echo ">> CORE_COMMIT is not set"; exit 1 )
	@[ "${UI_COMMIT}" ] || ( echo ">> UI_COMMIT is not set"; exit 1 )
	@[ "${TARGET_VERSION}" ] || ( echo ">> TARGET_VERSION is not set"; exit 1 )

	mkdir -p ${BUILD_CORE} ${BUILD_UI}

	git clone git@codeberg.org:usox/uxmp.git ${DIST_CORE} && cd ${DIST_CORE} && git checkout ${CORE_COMMIT} && composer install --no-dev -a
	sed -i "s/version = \"dev\"/version = \"${TARGET_VERSION}\"/" ${DIST_CORE}/config/config.dist.ini
	./${DIST_CORE}/bin/doctrine orm:generate-proxies
	cd ${DIST_CORE} && cp -r LICENSE README.md bin config src vendor resource ../../${BUILD_CORE}

	git clone git@codeberg.org:usox/uxmp-ui.git ${DIST_UI} && cd ${DIST_UI} && git checkout ${UI_COMMIT}
	sed -i "s/VITE_VERSION=dev/VITE_VERSION=${TARGET_VERSION}/" ${DIST_UI}/.env*
	cd ${DIST_UI} && npm install && npm run build
	cp -r ${DIST_UI}/dist/* ${BUILD_UI}

	cd build && tar -czf ${TARBALL} ${DIST_VERSION}

clean:
	rm -rf build/* dist/*
	
force:
